# theme

fpath+=$HOME/.zsh/pure
autoload -U colors && colors
#PS1="%B%{$fg[green]%}[%{$fg[green]%}%n%{$fg[green]%}@%{$fg[green]%}%M %{$fg[green]%}%~%{$fg[green]%}]%{$reset_color%}$%b "
setopt promptsubst
setopt PROMPT_SUBST

NEWLINE=$'\n'
#PROMPT="%{$fg[green]%}[%~]${NEWLINE}%{$fg[green]%}%B%{$fg[greeb]%}[%{$fg[green]%}%n%{$fg[green]%}@%{$fg[green]%}%M]%{$reset_color%}$%b "

autoload -U promptinit; promptinit
prompt pure

#prompt pure

#export NVM_DIR="$HOME/.config/zsh/nvm"
#source ~/.config/zsh/themes/bureau.zsh-theme

# History
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.cache/zsh/history

# Basic tab completion:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)

# vi mode
#source ~/.config/zsh/plugins/zsh-vim-mode/zsh-vim-mode.plugin.zsh

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

# Autosuggestions
source ~/.config/zsh/zsh-autosuggestions/zsh-autosuggestions.zsh

ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#444444"
ZSH_AUTOSUGGEST_STRATEGY=(history)

MODE_CURSOR_VICMD="green block"
MODE_CURSOR_VIINS="#20d08a blinking bar"
MODE_CURSOR_SEARCH="#ff00ff steady underline"

#bash insulter
if [ -f /home/beniamin/scripts/bash-insulter/src/bash.command-not-found ]; then
    . /home/beniamin/scripts/bash-insulter/src/bash.command-not-found
fi

#lf icons
#source ~/.config/diricons
#export LF_ICONS

# Aliases
alias config='/usr/bin/git --git-dir=/home/beniamin/dotfiles/ --work-tree=/home/beniamin'
alias ls='lsd -lah'
#alias ls='toolbox run lsd -ah'
#alias neovim='toolbox run neovim'
alias ytd='youtube-dl --audio-quality 0 --extract-audio --audio-format mp3'
alias p='sudo pacman'

#command not found handler
#source /usr/share/doc/pkgfile/command-not-found.zsh
#if [[ -s '/etc/zsh_command_not_found' ]]; then
#  source '/etc/zsh_command_not_found'
#fi


# Syntax highlighting
#source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

#ZSH_HIGHLIGHT_STYLES[command]=fg=033,bold

source /home/beniamin/.config/zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
