# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from libqtile.config import Key, Screen, Group, Drag, Click
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
import os
import subprocess

from typing import List  # noqa: F401

mod = "mod4"



keys = [
    # Switch between windows in current stack pane
    Key([mod], "k", lazy.layout.down()),
    Key([mod], "j", lazy.layout.up()),

    # Move windows up or down in current stack
    Key([mod, "shift"], "k", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "j", lazy.layout.shuffle_up()),

    # Switch window focus to other pane(s) of stack
    Key([mod], "space", lazy.window.toggle_floating()),

    # Swap panes of split stack
    Key([mod, "shift"], "space", lazy.layout.rotate()),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split()),
    Key([mod], "Return", lazy.spawn("st")),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout()),
    Key([mod], "q", lazy.window.kill()),

    Key([mod, "shift"], "r", lazy.restart()),
    Key([mod, "shift"], "e", lazy.shutdown()),
    Key([mod], "r", lazy.spawncmd()),
    #My Apps
    Key([mod], "w", lazy.spawn("firefox")),
    Key([mod], "p", lazy.spawn("pcmanfm")),
    Key(["mod1", "shift"], "4", lazy.spawn("flameshot gui")),
    Key([mod], "d", lazy.spawn("rofi -show drun -show-icons -font 'Fira Sans 10'")),
    
    #Media keys
    Key([],"XF86AudioRaiseVolume",lazy.spawn("pactl set-sink-volume 0 +5%")),
    Key([],"XF86AudioLowerVolume",lazy.spawn("pactl set-sink-volume 0 -5%")),
    Key([],"XF86MonBrightnessUp",lazy.spawn("light -A 10")),
    Key([],"XF86MonBrightnessDown",lazy.spawn("light -U 10")),

]

#groups = [Group(i) for i in "123456789"]

def init_group_names():
    return [("WWW", {'layout': 'monadtall'}),
            ("DEV", {'layout': 'monadtall'}),
            ("GAME", {'layout': 'floating'}),
            ("DOC", {'layout': 'monadtall'}),
            ("VBOX", {'layout': 'monadtall'}),
            ("CHAT", {'layout': 'monadtall'}),
            ("MUS", {'layout': 'monadtall'}),
            ("VID", {'layout': 'monadtall'}),
            ("GFX", {'layout': 'floating'})]

def init_colors():
     return [["#282a36", "#282a36"], # panel background
             ["#434758", "#434758"], # background for current screen tab
             ["#ffffff", "#ffffff"], # font color for group names
             ["#ff5555", "#ff5555"], # background color for layout widget
             ["#000000", "#000000"], # background for other screen tabs
             ["#808080", "#808080"], # dark green gradiant for other screen tabs
             ["#50fa7b", "#50fa7b"], # background color for network widget
             ["#00a3cc", "#00a3cc"], # background color for pacman widget
             ["#9AEDFE", "#9AEDFE"], # background color for cmus widget
             ["#000000", "#000000"], # background color for clock widget
             ["#434758", "#434758"]] # background color for systray widget
colors=init_colors()

#for i in groups:
#    keys.extend([
        # mod1 + letter of group = switch to group
#        Key([mod], i.name, lazy.group[i.name].toscreen()),

        # mod1 + shift + letter of group = switch to & move focused window to group
#        Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
#    ])

def init_groups():
    return [Group(name, **kwargs) for name, kwargs in group_names]

if __name__ in ["config","main"]:
    group_names=init_group_names()
    groups=init_groups()

for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name)))

layouts = [
    layout.MonadTall(margin=10),
    layout.Bsp(margin=10),
    layout.Max(),
    layout.Stack(num_stacks=2),
    layout.Floating()
]

widget_defaults = dict(
    font='Fira Sans',
    fontsize=12,
    padding=3,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.GroupBox(borderwidth=1,rounded = False,highlight_method="block",margin_y=0,margin_x=0,padding_y=4,padding_x=5,fontsize=10,inactive='#858585',this_screen_border=colors[0],other_current_screen_border=colors[5]),
                widget.Prompt(),
                widget.WindowName(),
                #widget.TextBox("default config", name="default"),
                widget.TextBox(text='',background = colors[0],foreground=colors[7],padding=0,fontsize=37),
                widget.Systray(foreground=colors[2],background=colors[7]),
                widget.TextBox(text='',background = colors[7],foreground=colors[5],padding=0,fontsize=37),
                widget.Battery(charge_char='', discharge_char='',full_char='',format='{char} {percent:2.0%}',unknown_char='',background=colors[5],foreground=colors[2]),
                widget.TextBox(text='',background = colors[5],foreground=colors[7],padding=0,fontsize=37),
                widget.Backlight(change_command='light -S',format='{percent: 2.0%}',foreground=colors[2],background=colors[7]),
                widget.TextBox(text='',background = colors[7],foreground=colors[5],padding=0,fontsize=37),
                widget.Clock(format='%Y-%m-%d %a %I:%M %p',background=colors[5],foreground=colors[2]),
                widget.TextBox(text='',background = colors[5],foreground=colors[7],padding=0,fontsize=37),
                widget.CurrentLayoutIcon(foreground=colors[2],background=colors[7]),
            ],
            20,
            opacity=0.7,
            background=colors[0],
        ),
    ),


    Screen(
        top=bar.Bar(
            [
                widget.GroupBox(borderwidth=1,rounded = False,highlight_method="block",margin_y=0,margin_x=0,padding_y=4,padding_x=5,fontsize=10,inactive='#858585',this_screen_border=colors[0],other_current_screen_border=colors[5]),
                widget.Prompt(),
                widget.WindowName(),
                #widget.TextBox("default config", name="default"),
                widget.TextBox(text='',background = colors[0],foreground=colors[7],padding=0,fontsize=37),
                widget.Systray(foreground=colors[2],background=colors[7]),
                widget.TextBox(text='',background = colors[7],foreground=colors[5],padding=0,fontsize=37),
                widget.Battery(charge_char='', discharge_char='',full_char='',format='{char} {percent:2.0%}',unknown_char='',background=colors[5],foreground=colors[2]),
                widget.TextBox(text='',background = colors[5],foreground=colors[7],padding=0,fontsize=37),
                widget.Backlight(change_command='light -S',format='{percent: 2.0%}',foreground=colors[2],background=colors[7]),
                widget.TextBox(text='',background = colors[7],foreground=colors[5],padding=0,fontsize=37),
                widget.Clock(format='%Y-%m-%d %a %H:%M %p',background=colors[5],foreground=colors[2]),
                widget.TextBox(text='',background = colors[5],foreground=colors[7],padding=0,fontsize=37),
                widget.CurrentLayoutIcon(foreground=colors[2],background=colors[7]),
            ],
            20,
            opacity=0.7,
            background=colors[0],
        ),
    ),

]

# Drag floating layouts.
mouse = [
	Drag([mod], "Button1", lazy.window.set_position_floating(),
		 start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
])
auto_fullscreen = True
focus_on_window_activation = "smart"


#HOOKS
@hook.subscribe.client_new
def floating(window):
        floating_types = ['notification', 'toolbar', 'splash', 'dialog']
        transient = window.window.get_wm_transient_for()
        if window.window.get_wm_type() in floating_types or transient:
            window.floating = True


@hook.subscribe.startup_once
def autostart():
        home = os.path.expanduser('~/.config/qtile/autostart.sh')
        subprocess.call([home])


# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
