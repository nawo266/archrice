#!/bin/sh
nitrogen --restore &
compton &
nm-applet &
blueberry-tray &
copyq &
dunst &
lxpolkit &
